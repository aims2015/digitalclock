package com.example.lynn.digitalclock;

import java.util.Calendar;
import java.util.TimeZone;

import static com.example.lynn.digitalclock.MainActivity.*;



/**
 * Created by lynn on 6/24/2015.
 */
public class ClockThread implements Runnable {
    private boolean keepGoing;

    public ClockThread() {
        keepGoing = true;

        new Thread(this).start();

    }

    public void pause(double seconds) {
        try {
            Thread.sleep((int) (seconds * 1000));
        } catch (InterruptedException ie) {
            System.out.println(ie);
        }
    }

    public void getTime() {
        Calendar c = Calendar.getInstance();

        String temp = timeZones.getSelectedItem().toString();

        c.setTimeZone(TimeZone.getTimeZone(temp));

        int hour = c.get(Calendar.HOUR_OF_DAY);

        final boolean am = hour < 12;

        hour -= (hour > 12) ? 12  : 0;

        int minute = c.get(Calendar.MINUTE);

        int second = c.get(Calendar.SECOND);

        final int hourFirstDigit = hour/10;
        final int hourSecondDigit = hour%10;

        final int minuteFirstDigit = minute/10;
        final int minuteSecondDigit = minute%10;

        final int secondFirstDigit = second/10;
        final int secondSecondDigit = second%10;

        views[0].post(new Runnable(){


            @Override
            public void run() {
                views[0].setImageDrawable(digits[hourFirstDigit]);
                views[1].setImageDrawable(digits[hourSecondDigit]);

                views[3].setImageDrawable(digits[minuteFirstDigit]);
                views[4].setImageDrawable(digits[minuteSecondDigit]);

                views[6].setImageDrawable(digits[secondFirstDigit]);
                views[7].setImageDrawable(digits[secondSecondDigit]);

                if (am)
                    views[8].setImageDrawable(MainActivity.am);
                else
                    views[8].setImageDrawable(pm);
            }
        });
    }

    @Override
    public void run() {
       while (keepGoing) {
           getTime();

           pause(0.5);
       }
    }

}
