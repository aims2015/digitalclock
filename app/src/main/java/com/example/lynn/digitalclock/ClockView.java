package com.example.lynn.digitalclock;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Arrays;
import java.util.TimeZone;

import static com.example.lynn.digitalclock.MainActivity.*;

/**
 * Created by lynn on 6/23/2015.
 */


public class ClockView extends LinearLayout {

    public ClockView(Context context) {
        super(context);

        digits = new Drawable[10];

        digits[0] = getResources().getDrawable(R.drawable.zero);
        digits[1] = getResources().getDrawable(R.drawable.one);
        digits[2] = getResources().getDrawable(R.drawable.two);
        digits[3] = getResources().getDrawable(R.drawable.three);
        digits[4] = getResources().getDrawable(R.drawable.four);
        digits[5] = getResources().getDrawable(R.drawable.five);
        digits[6] = getResources().getDrawable(R.drawable.six);
        digits[7] = getResources().getDrawable(R.drawable.seven);
        digits[8] = getResources().getDrawable(R.drawable.eight);
        digits[9] = getResources().getDrawable(R.drawable.nine);

        am = getResources().getDrawable(R.drawable.am);
        pm = getResources().getDrawable(R.drawable.pm);

        views = new ImageView[9];

        for (int counter=0;counter<views.length;counter++) {
            views[counter] = new ImageView(context);

            views[counter].setScaleX(5);
            views[counter].setScaleY(5);

            views[counter].setTranslationX(100 + counter*100);
            views[counter].setTranslationY(200);

            addView(views[counter]);
        }

        views[0].setImageDrawable(getResources().getDrawable(R.drawable.zero));
        views[1].setImageDrawable(getResources().getDrawable(R.drawable.zero));
        views[2].setImageDrawable(getResources().getDrawable(R.drawable.colon));
        views[3].setImageDrawable(getResources().getDrawable(R.drawable.zero));
        views[4].setImageDrawable(getResources().getDrawable(R.drawable.zero));
        views[5].setImageDrawable(getResources().getDrawable(R.drawable.colon));
        views[6].setImageDrawable(getResources().getDrawable(R.drawable.zero));
        views[7].setImageDrawable(getResources().getDrawable(R.drawable.zero));
        views[8].setImageDrawable(getResources().getDrawable(R.drawable.am));

        String[] temp = TimeZone.getAvailableIDs();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item, Arrays.asList(temp));

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        timeZones = new Spinner(context);

        timeZones.setAdapter(arrayAdapter);

        TextView label = new TextView(context);

        label.setText("Time Zone");

        label.setTranslationX(1000);
        label.setTranslationY(200);

        addView(label);

        timeZones.setTranslationX(1050);
        timeZones.setTranslationY(200);

        addView(timeZones);

        new ClockThread();

    }

}
